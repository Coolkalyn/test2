var contactsApp = angular.module("contactsApp", ['ui.router', 'ngStorage'])
contactsApp.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state('auth', {
            url: '/auth',
            abstract: true,
            template: '<ui-view>'
        })
        .state('/', {
            url: '/',
            templateUrl: 'main.html',
            controller: "ListController",
            resolve: {
                contacts: function(Contacts) {
                    return Contacts.getContacts();
                }
            },
            data: {
                'noLogin': true
            }
        })
        .state('#/contact/', {
            url: '/contact/',
            templateUrl: 'list.html',
            controller: "ListController",
            resolve: {
                contacts: function(Contacts) {
                    return Contacts.getContacts();
                }
            }
        })
        .state('otherwise', { url : '/contact/'});
        $urlRouterProvider.otherwise('/contact/');
}])

contactsApp.run([
    '$rootScope', '$state', '$stateParams', 'SessionService',
    function($rootScope, $state, $stateParams, SessionService) {

        $rootScope.$state = $state;
        $rootScope.$stateParams = $stateParams;

        $rootScope.user = null;

        // Здесь мы будем проверять авторизацию
        $rootScope.$on('$stateChangeStart',
            function(event, toState, toParams, fromState, fromParams) {
                SessionService.checkAccess(event, toState, toParams, fromState, fromParams);
            }
        );
    }
]);
