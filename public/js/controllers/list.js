contactsApp.controller("ListController", function(contacts, $scope, $location, Contacts, SessionService, $rootScope, $injector) {

    //show contacts
    $scope.contacts = contacts.data;

    $scope.$sessionStorage = $injector.get('$sessionStorage');

    //pagination
    $scope.$sessionStorage.currentPage = 0;
    $scope.pageSize = 9;
    $scope.data = contacts.data;
    $scope.numberOfPages = function() {
        return Math.ceil($scope.data.length / $scope.pageSize);
    }
    $scope.nextPage = function() {
        if ($scope.$sessionStorage.currentPage < $scope.numberOfPages() - 1) {
            return $scope.$sessionStorage.currentPage++;
        }
    }
    $scope.previousPage = function() {
        if ($scope.$sessionStorage.currentPage > 0) {
            return $scope.$sessionStorage.currentPage--;
        }
    }


    //Add new contact
    $scope.addContact = function(contact) {
        contact.salary += "$";
        var k = 0;
        for (i in $scope.contacts) {
            if ($scope.contacts[i].email == contact.email) {
                k++;
            }
        }
        if (k == 0) {
            if (contact.firstName !== undefined && contact.secondName !== undefined && contact.email !== undefined && contact.password !== undefined) {
                Contacts.createContact(contact).then(function(doc) {
                    $scope.contacts.push(doc.data);
                }, function(response) {
                    alert(response);
                });
                alert("Вы зарегистрировались!");
                $('#registerModal').modal('hide');
                $location.path("/contact/");
                $scope.$sessionStorage.currentPage = $scope.numberOfPages() - 1;
                $('#addModal').modal('hide');
                $scope.newUser = "";
            } else {
                alert("Заполните все недостающие поля");
            }
        } else {
            alert("Такой email уже существует");
        }
    }

    //delete contact
    $scope.deleteContact = function(contactId) {
        Contacts.deleteContact(contactId);
        for (i in $scope.contacts) {
            if ($scope.contacts[i]._id == contactId) {
                $scope.contacts.splice(i, 1);
            }
        }
        var currentPage = +$scope.$sessionStorage.currentPage + 1;
        if (currentPage > $scope.numberOfPages() && currentPage > 1) {
            $scope.$sessionStorage.currentPage--;
        }
    }

    //edit contact
    $scope.findContact = function(contactId) {
        Contacts.getContact(contactId).then(function(doc) {
            $scope.editedContact = doc.data;
        }, function(response) {
            alert(response);
        });
    }
    $scope.saveContact = function(contact) {
        if (contact.firstName !== undefined && contact.secondName !== undefined && contact.email !== undefined && contact.password !== undefined && contact.firstName !== "" && contact.secondName !== "" && contact.email !== "" && contact.password !== "") {
            Contacts.editContact(contact);
            for (i in $scope.contacts) {
                if ($scope.contacts[i]._id == contact._id) {
                    $scope.contacts[i] = contact;
                }
            }
            $('#editModal').modal('hide');
            $scope.editedContact = "";
        }
    }


    //changePassword
    $scope.changePassword = function(contact, editedContact) {
        if (contact.oldPassword != editedContact.password) {
            alert("Вы ввели неверно старый пароль");
        } else {
            if (contact.password != contact.password2) {
                alert("Повторите верно новый пароль");
            } else {
                editedContact.password = contact.password;
                Contacts.editContact(editedContact);
                for (i in $scope.contacts) {
                    if ($scope.contacts[i]._id == contact._id) {
                        $scope.contacts[i] = editedContact;
                    }
                }
                alert("Вы успешно изменили свой пароль");
                $('#changePasswordModal').modal('hide');
                $scope.contact = "";
            }
        }
    }


    //Login
    $scope.login = function(user) {
        $scope.log = "";
        for (i in $scope.contacts) {
            if ($scope.contacts[i].email == user.email) {
                $scope.log = $scope.contacts[i];
            }
        }
        if ($scope.log || user.email == "admin@admin") {
            if ($scope.log.password == user.password) {
                $scope.$sessionStorage.user = $scope.log;
                $('#loginModal').modal('hide');
                $location.path("/contact/");
            } else {
                if (user.email == "admin@admin" && user.password == "admin") {
                    user.firstName = "Admin";
                    $scope.$sessionStorage.user = user;
                    $('#loginModal').modal('hide');
                    $location.path("/contact/");
                } else {
                    alert("Вы ввели неправильный пароль");
                }
            }
        } else {
            alert("Вы ввели неправильный email");
        }
    }


    //Logout
    $scope.logOut = function() {
        $scope.$sessionStorage.user = undefined;
        $location.path("/");
    }


    //CheckforAdmin
    $scope.isAdmin = function() {
        if ($scope.$sessionStorage.user !== undefined) {
            $location.path("/contact/");
            if ($scope.$sessionStorage.user.email == "admin@admin" && $scope.$sessionStorage.user.password == "admin") {
                $scope.pageSize = 6;
                return true;
            } else {
                $scope.pageSize = 8;
                return false;
            }
        } else {
            $location.path("/");
        }
    }


    //checkForUser
    $scope.isLoggedIn = function() {
        if ($scope.$sessionStorage.user !== undefined) {
            return true;
        } else {
            return false;
        }
    }

    //getCurrentUser
    $scope.getCurrentUser = function() {
        return $scope.$sessionStorage.user
    }


    //searchField
    $scope.search = function(item) {
        if (!$scope.$sessionStorage.query || (item.firstName.toLowerCase().indexOf($scope.$sessionStorage.query.toLowerCase()) != -1) || (item.secondName.toLowerCase().indexOf($scope.$sessionStorage.query.toLowerCase()) != -1)) {
            return true;
        }
        return false;
    }

});
