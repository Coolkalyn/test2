
var MongoClient = require('mongodb').MongoClient
 , assert = require('assert');

// Connection URL
var url = 'mongodb://localhost:27017/test';
// Use connect method to connect to the Server
MongoClient.connect(url, function(err, db) {
  assert.equal(null, err);
  console.log("Connected correctly to server");
    // Insert multiple documents
    db.collection('contacts').insertMany([
      {
        'email': "jobmail1@gmail.com",
        'password': "parol1",
        'firstName': "Вася",
        'secondName': "Пяточкин",
        'position': "Programmer",
        'salary': "100",
        'phoneNumber': "095-432-10-01",
        'birthday': "10.12.1992",
        'Info': ""
      }, {
        'email': "jobmail2@gmail.com",
        'password': "parol2",
        'firstName': "Петя",
        'secondName': "Сидоров",
        'position': "Programmer",
        'salary': "500",
        'phoneNumber': "095-432-10-02",
        'birthday': "05.06.1994",
        'Info': ""
      }, {
        'email': "jobmail3@gmail.com",
        'password': "parol3",
        'firstName': "Оля",
        'secondName': "Самойлова",
        'position': "Programmer",
        'salary': "900",
        'phoneNumber': "095-432-10-03",
        'birthday': "14.11.1996",
        'Info': ""
      }, {
        'email': "jobmail4@gmail.com",
        'password': "parol4",
        'firstName': "Вася",
        'secondName': "Пушкин",
        'position': "Programmer",
        'salary': "300",
        'phoneNumber': "095-432-10-04",
        'birthday': "23.10.1991",
        'Info': ""
      }, {
        'email': "jobmail5@gmail.com",
        'password': "parol5",
        'firstName': "Степан",
        'secondName': "Пяточкин",
        'position': "Programmer",
        'salary': "600",
        'phoneNumber': "095-432-10-05",
        'birthday': "16.01.1990",
        'Info': ""
      }
    ], function(err, r) {
      assert.equal(null, err);
      assert.equal(5, r.insertedCount);
      console.log("inserted files!");
      db.close();
  });
});
